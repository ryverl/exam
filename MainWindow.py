#! /usr/bin/env python
# -*- coding:utf-8 -*-

from PyQt4 import QtGui
from TabWidget import TabWidget
from ConfigDlg import ConfigDlg

from Db import Db
from Db.RadioGroup import RadioGroup,IllStatus
from Db.CheckBox import CheckBox
from Db.TrueFalse import TrueFalse
from Db.Config import Config

import qrc

class MainWindow(QtGui.QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.initUI()

        self.tabWidget.signal.setStatusMsg.connect(self.statusBar().showMessage)
        #TODO: need deleted
        self.tabWidget.signal.setStatusMsg.emit("hello")

    def initUI(self):
        db = Db("data.db")
        try:
            self.dbRadioBox = RadioGroup(db)
            self.dbCheckBox = CheckBox(db)
            self.dbTrFls = TrueFalse(db)
            self.dbConfig = Config(db)
        except IllStatus:
            #TODO
            pass

        self.showMaximized()
        self.initBar()

        self.tabWidget = TabWidget(self.dbRadioBox,
                                   self.dbCheckBox,
                                   self.dbTrFls,
                                   self.dbConfig)
        self.setCentralWidget(self.tabWidget)

    def initBar(self):
        configAct = QtGui.QAction(QtGui.QIcon(':/config'), self.tr('Config(&C)...'), self)
        configAct.triggered.connect(self.config)

        aboutAct = QtGui.QAction(QtGui.QIcon(':/about'), self.tr("About(&A)"), self)
        aboutAct.triggered.connect(self.about)

        menuBar = self.menuBar()
        settingMenu = menuBar.addMenu(self.tr("Settings(&S)"))
        settingMenu.addAction(configAct)
        helpMenu = menuBar.addMenu(self.tr("Help(&H)"))
        helpMenu.addAction(aboutAct)

        toolBar = self.addToolBar('Help')
        toolBar.addAction(configAct)
        toolBar.addAction(aboutAct)

    def about(self):
        msg= u"%s\n\n%s\n%s" % (self.tr("Devoted to my wife: Cosine"),
                             self.tr("The source of this project is at:"),
                             u"https://bitbucket.org/compassym/exam")
        QtGui.QMessageBox.about(self, self.tr("About"), msg)

    def config(self):
        configDialg = ConfigDlg(self.dbRadioBox,
                                self.dbCheckBox,
                                self.dbTrFls,
                                self.dbConfig)
        configDialg.exec_()
