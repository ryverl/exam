#!/usr/bin/env python
# -*- coding:utf-8 -*-

from PyQt4 import QtGui, QtCore

class Signal(QtCore.QObject):
    slctNewKind = QtCore.pyqtSignal(int)

class QstKindSlct(QtGui.QWidget):
    '''
    用于选择题型
    '''

    def __init__(self, parent=None):
        super(QstKindSlct, self).__init__()
        self.signal = Signal()
        self.initUI()

    def initUI(self):
        radioLib = QtGui.QPushButton(self.tr("Single selection"))
        checkLib = QtGui.QPushButton(self.tr("Multiple selection"))
        trFlsLib = QtGui.QPushButton(self.tr("True Or False"))
        radioLib.setCheckable(True)
        checkLib.setCheckable(True)
        trFlsLib.setCheckable(True)
        radioLib.setAutoExclusive(True)
        checkLib.setAutoExclusive(True)
        trFlsLib.setAutoExclusive(True)

        radioLib.setChecked(True)

        self.kindBtnGrp = QtGui.QButtonGroup()
        self.kindBtnGrp.addButton(radioLib, 0)
        self.kindBtnGrp.addButton(checkLib, 1)
        self.kindBtnGrp.addButton(trFlsLib, 2)
        self.kindBtnGrp.buttonClicked[int].connect(self.newKindSlcted)

        spacerItem = QtGui.QSpacerItem(self.width()*3/4, 1,
                                       QtGui.QSizePolicy.Expanding)
        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(radioLib)
        hLayout.addWidget(checkLib)
        hLayout.addWidget(trFlsLib)
        hLayout.addItem(spacerItem)

        self.setLayout(hLayout)

    #以下两个函数的区别是：
    #newKindSlcted响应用户点击按钮的动作
    #setNewKind用于被外界调用
    def newKindSlcted(self, kind):
        self.signal.slctNewKind.emit(kind)

    def setNewKind(self, kind):
        self.kindBtnGrp.button(kind).setChecked(True)
        self.newKindSlcted(kind)

    def checkedId(self):
        return self.kindBtnGrp.checkedId()

