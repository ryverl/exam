#! /usr/bin/env python
#-*- coding:utf-8 -*-

'''
用于实现一个可供用户选择试题编号的列表
'''

from PyQt4 import QtGui, QtCore

class Signal(QtCore.QObject):
    #prevId, newId
    selectNewQuestion = QtCore.pyqtSignal(int, int)


class IdListWidget(QtGui.QWidget):
    cellWidth  = 30
    cellHeight = 30

    def __init__(self, cellCount = 0, parent=None):
        super(IdListWidget, self).__init__(parent)
        self.signal = Signal()

        self.cellPerRow = 1
        self.rowsCnt = 1
        self.cellCount = cellCount
        self.selectedCellId = 0
        self.tagedCells = []
        #每一行最小能容纳5个单元
        self.setMinimumWidth(IdListWidget.cellWidth*5)
        self.setSizePolicy(QtGui.QSizePolicy.Minimum,
                           QtGui.QSizePolicy.Minimum)

    def setGridPara(self):
        self.cellPerRow = self.width()/IdListWidget.cellWidth
        rowsCnt =  (self.cellCount + self.cellPerRow - 1)/self.cellPerRow
        if rowsCnt != self.rowsCnt:
            self.rowsCnt = rowsCnt
            self.setMinimumHeight(self.rowsCnt*IdListWidget.cellHeight + 20)
        if self.cellCount==0:
            self.rowsCnt = 1
        self.gridLeft = (self.width()-IdListWidget.cellWidth * self.cellPerRow)/2
        self.gridRight = self.gridLeft + IdListWidget.cellWidth * self.cellPerRow
        self.gridTop = (self.height()-IdListWidget.cellHeight * self.rowsCnt)/2
        self.gridButtom = self.gridTop + IdListWidget.cellHeight * self.rowsCnt

    def paintEvent(self, e):
        self.setGridPara()

        painter = QtGui.QPainter()
        painter.begin(self)
        #画横线
        for i in range(0, self.rowsCnt+1):
            painter.drawLine(self.gridLeft, self.gridTop + i*IdListWidget.cellHeight,
                             self.gridRight, self.gridTop + i*IdListWidget.cellHeight)
        #画竖线
        for i in range(0, self.cellPerRow+1):
            painter.drawLine(self.gridLeft + i*IdListWidget.cellWidth, self.gridTop,
                             self.gridLeft + i*IdListWidget.cellWidth, self.gridButtom)
        #题号
        for i in  range(0, self.cellCount):
            left = self.gridLeft + (i%self.cellPerRow)*IdListWidget.cellWidth
            top = self.gridTop + (i/self.cellPerRow)*IdListWidget.cellHeight
            painter.drawText(QtCore.QRectF(left, top,
                                           IdListWidget.cellWidth, IdListWidget.cellHeight),
                             QtCore.Qt.AlignCenter,
                             QtCore.QString.number(i+1))

        color = QtGui.QColor(244, 244, 100)
        for i in self.tagedCells:
            self.setColorOnCell(painter, i, color)

        if self.cellCount>0:
            self.setColorOnCell(painter)

        painter.end()


    def setColorOnCell(self, painter, cellId=None, color=QtGui.QColor(54, 214,249)):
        if cellId==None:
            cellId = self.selectedCellId
        if cellId<0 or cellId > self.cellCount-1:
            return
        row = (cellId)/self.cellPerRow
        column = (cellId)%self.cellPerRow
        left = self.gridLeft + column*IdListWidget.cellWidth
        top = self.gridTop + row*IdListWidget.cellHeight
        rect = QtCore.QRectF(left, top, IdListWidget.cellWidth, IdListWidget.cellHeight)
        painter.setBrush(QtGui.QBrush(color))
        painter.drawRect(rect)
        painter.drawText(rect, QtCore.Qt.AlignCenter,
                         QtCore.QString.number(cellId+1))


    def mousePressEvent(self, e):
        x = e.x()
        y = e.y()
        if (x>self.gridLeft and x<self.gridRight and
            y>self.gridTop  and y<self.gridButtom):
            selectedCellId = (y-self.gridTop)/IdListWidget.cellHeight * self.cellPerRow + \
                             (x-self.gridLeft)/IdListWidget.cellWidth
            if selectedCellId<self.cellCount:
                self.setSelectCell(selectedCellId)

    def setSelectCell(self, cellId):
        prevId = self.selectedCellId
        self.selectedCellId = cellId
        self.signal.selectNewQuestion.emit(prevId, self.selectedCellId)
        self.update()


    def setCellCnt(self, cnt):
        self.cellCount = cnt
        self.update()

    def getCellCnt(self):
        return self.cellCount

    def getSelectedCellId(self):
        return self.selectedCellId

    def sizeHint(self):
        return QtCore.QSize(IdListWidget.cellWidth*10+10, IdListWidget.cellHeight*2+10)

    def setTagOnCell(self, cellId):
        if not cellId in self.tagedCells:
            self.tagedCells.append(cellId)
            self.update()

    def setTagsAll(self):
        self.tagedCells = range(0, self.cellCount)
        self.update()

    def rstTagOnCell(self, cellId):
        if cellId in self.tagedCells:
            self.tagedCells.remove(cellId)
            self.update()

    def cleanTags(self):
        self.tagedCells = []
        self.update()
