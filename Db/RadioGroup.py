#!/usr/bin/env python
#-*- coding:utf-8 -*-

from  Question import Question
from Question import IllParaErr, IllStatus

class RadioGroup(Question):
    '''
    用于与单选题库交互
    答案0-3分别对应A-D
    答案小于0或者大于3为非法
    '''

    def __init__(self, db ):
        Question.__init__(self, db, "RadioGroup", 0, 3)

