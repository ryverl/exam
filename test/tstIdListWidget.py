#! /usr/bin/env python
# -*- coding:utf-8 -*-


import sys
sys.path.append('../')
sys.path.append('./')

from PyQt4 import QtGui
from IdListWidget import IdListWidget

app = QtGui.QApplication(sys.argv)
widget = QtGui.QWidget()
idListWidget = IdListWidget(3)
hLayout = QtGui.QHBoxLayout()
hLayout.addWidget(idListWidget)
widget.setLayout(hLayout)
widget.show()
sys.exit(app.exec_())
