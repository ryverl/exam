#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import sys
sys.path.append('../..')

import Db
import Db.CheckBox

db = Db.Db('data.db')
try:
    db.cursor.execute('DELETE  FROM CheckBox')
    db.commit()
except sqlite3.OperationalError:
    pass
checkbox = Db.CheckBox.CheckBox(db)
assert(checkbox.count()==0)
assert(checkbox.countintrash()==0)
assert(checkbox.totalcount()==0)
print('After created: Right!')

checkbox.insert(u'问题1', [0])
assert(checkbox.count()==1)
assert(checkbox.countintrash()==0)
assert(checkbox.totalcount()==1)
print('After inserted: Right!')

checkbox.insertmany([(u'问题2', [   1      ]),
                     (u'问题3', [0, 1      ]),
                     (u'问题4', [      2   ]),
                     (u'问题5', [0,    2   ]),
                     (u'问题6', [   1, 2   ]),
                     (u'问题7', [0, 1, 2   ])])
assert(checkbox.count()==7)
assert(checkbox.countintrash()==0)
assert(checkbox.totalcount()==7)
print('After insertmany: Right!')

checkbox.movetotrash(2)
checkbox.movetotrashmany([3, 4, 5, 6])
assert(checkbox.count()==2)
assert(checkbox.countintrash()==5)
assert(checkbox.totalcount()==7)
print('After movetotrash: Right!')

checkbox.recover(3)
checkbox.recovermany([2, 4])
assert(checkbox.count()==5)
assert(checkbox.countintrash()==2)
assert(checkbox.totalcount()==7)
print('After recover: Right!')

checkbox.delete(2)
checkbox.deletemany([2, 6])
assert(checkbox.count()==4)
assert(checkbox.countintrash()==1)
assert(checkbox.totalcount()==5)
print('After delete: Right!')

assert(checkbox.maxid()==7)
assert(checkbox.minid()==1)
print('maxid() and minid() right!')

item1 = checkbox.fetch(1)
item2 = checkbox.fetch(2)
assert(item1[0]==1)
assert(item1[1]==u'问题1')
assert(item1[2]==[0])
assert(item1[3]==0)
assert(item2 == None)
print('fetch() right!')

items=checkbox.fetchall()
assert(items[0][0]==1)
assert(items[0][1]==u'问题1')
assert(items[0][2]==[0])
assert(items[0][3]==0)
assert(items[1][0]==3)
assert(items[1][1]==u'问题3')
assert(items[1][2]==[0,1])
assert(items[1][3]==0)
assert(items[2][0]==4)
assert(items[3][0]==7)
print('fetchall() right!')

items=checkbox.fetchallintrash()
assert(items[0][0]==5)
assert(items[0][3]==1)
print('fetchallintrash() right!')

checkbox.modifymany([(1, u'question1', [1, 2]),
                     (3, u'question3', [0, 2, 3]) ])
checkbox.modify(5, u'question5', [1, 2])
item1 = checkbox.fetch(1)
item2 = checkbox.fetch(3)
item3 = checkbox.fetch(5)
assert(item1[1]==u'question1')
assert(item2[1]==u'question3')
assert(item3[1]==u'question5')
assert(item1[2]==[1,2])
assert(item2[2]==[0,2,3])
assert(item3[2]==[1,2])
print('modify() and modifymany() right')

try:
    checkbox.modify(1, u'question1', [0, 1, 2, 3, 4])
    print(u'对错书的答案个数不能正确处理')
except Db.CheckBox.ErrAnswer as e:
    assert(e.key==0)
    print(u"可以正确处理错误的答案个数")


try:
    checkbox.modify(1, u'question1', [0, 5, 2])
    print(u"对错误的答案取值范围不能正确处理？")
except Db.CheckBox.ErrAnswer as e:
    assert(e.key==1)
    print(u"可以正确处理错误的答案取值范围")

db.close()
