#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
sys.path.append('./')
sys.path.append('../')

from PyQt4 import QtGui,QtCore

import  MainWindow


app = QtGui.QApplication(sys.argv)
translator = QtCore.QTranslator()
translator.load('zh_CN.qm')
app.installTranslator(translator)
mainWindow = MainWindow.MainWindow()
mainWindow.show()
sys.exit(app.exec_())
