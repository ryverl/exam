<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>ConfigDlg</name>
    <message>
        <location filename="ConfigDlg.py" line="17"/>
        <source>Config</source>
        <translation>配置试题参数</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="37"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="38"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="59"/>
        <source>Score Parameter</source>
        <translation>分值</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="89"/>
        <source>Number Of Exercisers</source>
        <translation>数量分配</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="107"/>
        <source>&quot;SingleSelect&quot;</source>
        <translation>&quot;单选&quot;</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="108"/>
        <source>&quot;MultiSelect&quot;</source>
        <translation>&quot;多选&quot;</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="109"/>
        <source>&quot;TrueOrFalse&quot;</source>
        <translation>&quot;判断&quot;</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="117"/>
        <source>There are not enouth number of</source>
        <translation>试题库中的以下题型数量不够：</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="120"/>
        <source> exercisers, So I will use</source>
        <translation>。其对应的题量为：</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="124"/>
        <source> respectively!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="125"/>
        <source>, Are you sure?</source>
        <translation>。  使用上述值代替请按“yes”，修改请按&quot;no&quot;，需要其他动作请联系作者。</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="93"/>
        <source>SingleSelect:</source>
        <translation>单选：</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="94"/>
        <source>MultiSelect:</source>
        <translation>多选：</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="95"/>
        <source>TrueOrFalse:</source>
        <translation>判断：</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="71"/>
        <source>In Fact, the score parameter above
</source>
        <translation>实际上，上面关于分数的设置 
</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="72"/>
        <source>is only a reference value. When the
 </source>
        <translation>仅仅作为一个参考值，如果您
 </translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="73"/>
        <source>parameters you set makes the total 
</source>
        <translation>设置分数与个题型数量使得总
</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="74"/>
        <source>score not equal to 100, I will adjust
</source>
        <translation>分不能为100，那么我将适当
</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="75"/>
        <source>it, but keep the ratio of the scores
</source>
        <translation>调整不同题型的分值，但保持
</translation>
    </message>
    <message>
        <location filename="ConfigDlg.py" line="76"/>
        <source>as you set.</source>
        <translation>各题型分值比例与您设置一致。</translation>
    </message>
</context>
<context>
    <name>Exam</name>
    <message>
        <location filename="Exam.py" line="30"/>
        <source>Product Exercises</source>
        <translation>开始考试</translation>
    </message>
    <message>
        <location filename="Exam.py" line="34"/>
        <source>&lt;&lt; Pre</source>
        <translation>&lt;&lt;上一题</translation>
    </message>
    <message>
        <location filename="Exam.py" line="35"/>
        <source>Next &gt;&gt;</source>
        <translation>下一题 &gt;&gt;</translation>
    </message>
    <message>
        <location filename="Exam.py" line="36"/>
        <source>Commit</source>
        <translation>交卷</translation>
    </message>
    <message>
        <location filename="Exam.py" line="126"/>
        <source>Congratuations! You got: </source>
        <translation>恭喜！您的得分为：</translation>
    </message>
    <message>
        <location filename="Exam.py" line="128"/>
        <source>You got: </source>
        <translation>您的得分为：</translation>
    </message>
    <message>
        <location filename="Exam.py" line="233"/>
        <source>Index in exam:   </source>
        <translation>题号：              </translation>
    </message>
    <message>
        <location filename="Exam.py" line="232"/>
        <source>Index in Library:</source>
        <translation>本题在题库中的编号：</translation>
    </message>
    <message>
        <location filename="Exam.py" line="234"/>
        <source>Answer:          </source>
        <translation>答案：              </translation>
    </message>
</context>
<context>
    <name>Insert</name>
    <message>
        <location filename="Insert.py" line="35"/>
        <source>Commit</source>
        <translation>提交</translation>
    </message>
    <message>
        <location filename="Insert.py" line="46"/>
        <source>Please input the whole question:</source>
        <translation>请在下面的文本框中输入题干：</translation>
    </message>
    <message>
        <location filename="Insert.py" line="48"/>
        <source>Which one is the answer?</source>
        <translation>本题标准答案是？</translation>
    </message>
    <message>
        <location filename="Insert.py" line="56"/>
        <source>You havn&apos;t input the question!</source>
        <translation>不好意思，您还没有输入合适的题干呢!</translation>
    </message>
    <message>
        <location filename="Insert.py" line="62"/>
        <source>You havn&apos;t input the answer!</source>
        <translation>请您为这道题提供一个正确的答案!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.py" line="50"/>
        <source>About(&amp;A)</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="56"/>
        <source>Help(&amp;H)</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="67"/>
        <source>About</source>
        <translation>关于ccExam</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="64"/>
        <source>The source of this project is at:</source>
        <translation>本项目源代码位于：</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="64"/>
        <source>Devoted to my wife: Cosine</source>
        <translation>献给我的爱妻：大漠</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="47"/>
        <source>Config(&amp;C)...</source>
        <translation>设置分值与题量(&amp;C)...</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="54"/>
        <source>Settings(&amp;S)</source>
        <translation>设置(&amp;S)</translation>
    </message>
</context>
<context>
    <name>QstKindSlct</name>
    <message>
        <location filename="QstKindSlct.py" line="20"/>
        <source>Single selection</source>
        <translation>单选题</translation>
    </message>
    <message>
        <location filename="QstKindSlct.py" line="21"/>
        <source>Multiple selection</source>
        <translation>多选题</translation>
    </message>
    <message>
        <location filename="QstKindSlct.py" line="22"/>
        <source>True Or False</source>
        <translation>判断题</translation>
    </message>
</context>
<context>
    <name>Scan</name>
    <message>
        <location filename="Scan.py" line="96"/>
        <source>Question:</source>
        <translation>问题:</translation>
    </message>
    <message>
        <location filename="Scan.py" line="97"/>
        <source>Answer:</source>
        <translation>答案:</translation>
    </message>
    <message>
        <location filename="Scan.py" line="107"/>
        <source>True</source>
        <translation>正确</translation>
    </message>
    <message>
        <location filename="Scan.py" line="107"/>
        <source>False</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="Scan.py" line="89"/>
        <source>I&apos;m sorry for hav&apos;nt find the exercise with id:</source>
        <translation>啊？我很努力的找了，但确实没找到这道题：</translation>
    </message>
    <message>
        <location filename="Scan.py" line="90"/>
        <source>Maybe you have delete this question?</source>
        <translation>或许您已经把这道题删掉了？</translation>
    </message>
    <message>
        <location filename="Scan.py" line="35"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="Scan.py" line="36"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="Scan.py" line="64"/>
        <source>Question Id:</source>
        <translation>题号:</translation>
    </message>
    <message>
        <location filename="Scan.py" line="133"/>
        <source>The excercise is deleted!</source>
        <translation>本题已从题库中删除！</translation>
    </message>
    <message>
        <location filename="Scan.py" line="80"/>
        <source>There is no nothing in the library yet, </source>
        <translation>啊？试题库中好像没有这一类题型呢……</translation>
    </message>
    <message>
        <location filename="Scan.py" line="81"/>
        <source>you can add some exercise in &apos;Insert Into Library&apos; tab </source>
        <translation>您可以在“录入新题”选项卡中加入新的试题。</translation>
    </message>
</context>
<context>
    <name>StackAnsWidget</name>
    <message>
        <location filename="StackAnsWidget.py" line="83"/>
        <source>True</source>
        <translation>正确</translation>
    </message>
    <message>
        <location filename="StackAnsWidget.py" line="84"/>
        <source>False</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="TabWidget.py" line="26"/>
        <source>Local Exam</source>
        <translation>本地考试</translation>
    </message>
    <message>
        <location filename="TabWidget.py" line="45"/>
        <source>Scan Library</source>
        <translation>查看题库</translation>
    </message>
    <message>
        <location filename="TabWidget.py" line="46"/>
        <source>Insert Into Library</source>
        <translation>录入新题</translation>
    </message>
</context>
</TS>
