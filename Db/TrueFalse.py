#!/usr/bin/env python
#-*- coding:utf-8 -*-

from  Question import Question
from Question import IllParaErr, IllStatus

class TrueFalse(Question):
    '''
    用于与判断题库交互
    答案0表示错误
    答案1表示正确
    答案小于0或者大于1为非法
    '''

    def __init__(self, db ):
        Question.__init__(self, db, "TrueFalse", 0, 1)
