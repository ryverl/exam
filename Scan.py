#! /usr/bin/env python
#-*- coding:utf-8 -*-

from PyQt4 import QtGui, QtCore
from IdListWidget import IdListWidget
from QstKindSlct import QstKindSlct

class Signal(QtCore.QObject):
    #第一个参数确定单选/多选/判断
    #第二个参数确定题号
    modifyQst = QtCore.pyqtSignal(int, int)
    deleteSomeExer = QtCore.pyqtSignal()

class Scan(QtGui.QWidget):
    '''
    浏览试题库
    '''

    def __init__(self, dbRadioGrp, dbCheckBox, dbTrFls, parent=None):
        super(Scan, self).__init__(parent)

        self.signal = Signal()

        self.dbRadioGrp = dbRadioGrp
        self.dbCheckBox= dbCheckBox
        self.dbTrFls= dbTrFls
        self.dbTable=[self.dbRadioGrp, self.dbCheckBox, self.dbTrFls]

        self.initUI()

    def initUI(self):
        self.qstKindSlct = QstKindSlct()
        self.qstScan = QtGui.QLabel()
        self.idListWidget = IdListWidget(0)
        self.modifyBtn = QtGui.QPushButton(self.tr("Modify"))
        self.deleteBtn = QtGui.QPushButton(self.tr("Delete"))

        self.slctNewKind(0)
        self.setQstContent(self.getSelectedCellId())
        self.modifyBtn.setFixedWidth(100)
        self.modifyBtn.clicked.connect(self.modifyQst)
        self.deleteBtn.setFixedWidth(100)
        self.deleteBtn.clicked.connect(self.deleteQst)

        self.qstKindSlct.signal.slctNewKind[int].connect(self.slctNewKind)
        self.qstScan.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.idListWidget.signal.selectNewQuestion.\
                                 connect(self.selectNewQuestion)

        self.scroll = QtGui.QScrollArea()
        self.scroll.setWidget(self.qstScan)
        self.scroll.setWidgetResizable(True)

        vLayout = QtGui.QVBoxLayout()
        vLayout.addWidget(self.qstKindSlct)
        vLayout.addWidget(self.scroll)
        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(self.modifyBtn)
        hLayout.addItem(QtGui.QSpacerItem(20,1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(self.deleteBtn)
        hLayout.addItem(QtGui.QSpacerItem(20,1, QtGui.QSizePolicy.Expanding))
        vLayout.addLayout(hLayout)
        vLayout.addItem(QtGui.QSpacerItem(1,20, QtGui.QSizePolicy.Fixed))
        vLayout.addWidget(QtGui.QLabel(self.tr("Question Id:")))
        vLayout.addWidget(self.idListWidget)
        self.setLayout(vLayout)

    def slctNewKind(self, kind):
        cnt = self.dbTable[kind].maxid()
        self.idListWidget.setCellCnt(cnt)
        if cnt>0:
            self.idListWidget.setSelectCell(0)
        else:
            self.idListWidget.setSelectCell(-1)

    def setQstContent(self, qstId):
        #数据库中ID从1开始计数。
        if qstId == 0:
            if self.dbTable[self.qstKindSlct.checkedId()].count()==0:
                label1 = self.tr("There is no nothing in the library yet, ")
                label2 = self.tr(
                        "you can add some exercise in 'Insert Into Library' tab ")
                self.qstScan.setText(u"%s \n\n%s\n" % (label1, label2))
                self.modifyBtn.setEnabled(False)
                self.deleteBtn.setEnabled(False)
                return
        record = self.dbTable[self.qstKindSlct.checkedId()].fetch(qstId)
        if record == None:
            label1 = self.tr("I'm sorry for hav'nt find the exercise with id:")
            label2 = self.tr("Maybe you have delete this question?")
            self.qstScan.setText(u"%s %s\n\n%s\n" % (label1, qstId, label2))
            self.modifyBtn.setEnabled(True)
            self.deleteBtn.setEnabled(False)
            return
        content = "%s" % record[1]
        label1 = self.tr("Question:")
        label2 = self.tr("Answer:")
        content = u"%s\n\n%s\n\n%s    %s" % \
                  (label1, content, label2, self.decodeAnswer(record))
        self.qstScan.setText(content)
        self.modifyBtn.setEnabled(True)
        self.deleteBtn.setEnabled(True)

    def decodeAnswer(self, record):
        answerforread = [['A', 'B', 'C', 'D'],
                         ['A', 'B', 'C', 'D'],
                         [self.tr("True"), self.tr("False")]]
        if self.qstKindSlct.checkedId() == 1:
            ret = ""
            for i in record[2]:
                ret = "%s %s" % (ret, answerforread[1][i])
            return ret
        else:
            return answerforread[self.qstKindSlct.checkedId()][record[2]]

    def modifyQst(self):
        self.signal.modifyQst.emit(self.qstKindSlct.checkedId(),
                                   self.getSelectedCellId())

    def databaseUpdate(self):
        cnt = self.dbTable[self.qstKindSlct.checkedId()].maxid()
        selectInit = False
        if cnt>0 and self.idListWidget.getCellCnt()==0:
            selectInit = True
        self.idListWidget.setCellCnt(cnt)
        self.setQstContent(self.getSelectedCellId())
        if selectInit:
            self.selectNewQuestion(1)

    def deleteQst(self):
        self.dbTable[self.qstKindSlct.checkedId()].\
                delete(self.getSelectedCellId())
        self.qstScan.setText(self.tr("The excercise is deleted!"))
        self.signal.deleteSomeExer.emit()
        self.deleteBtn.setEnabled(False)

    def getSelectedCellId(self):
        #数据库中的ID从1开始计数
        #而IdListWidget中的ID从0开始计数
        return self.idListWidget.getSelectedCellId()+1

    def selectNewQuestion(self, prevQstId, newQstId):
        #数据库中的ID从1开始计数
        #而IdListWidget中的ID从0开始计数
        self.setQstContent(newQstId+1)
