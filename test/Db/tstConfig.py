#!/usr/bin/env python
# -*- coding:utf-8 -*-


import sys
sys.path.append('./')
sys.path.append('../../')

from Db import Db
from Db.Config import Config

db = Db('data.db')
config = Config(db)

config.setkey('ip', '192.168.1.1')
config.setkey('lang', 'zh_CN')
assert(config.readkey('ip')=='192.168.1.1')
assert(config.readkey('lang')=='zh_CN')
config.setkey('ip', '192.168.1.2')
assert(config.readkey('ip')=='192.168.1.2')
assert(config.readkey('nokey')==None)

print('All Right')

