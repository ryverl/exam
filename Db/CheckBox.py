#!/usr/bin/env python
#-*- coding:utf-8 -*-

from  Question import Question
from Question import IllParaErr, IllStatus

class ErrAnswer(Exception):
    def __init__(self, msg, key=0):
        '''
        key=0表示答案个数多余4个或者少于1个
        key=1表示部分答案取值范围不属于[0,3]
        '''
        Exception.__init__(self, msg)
        self.key=key

class CheckBox(Question):
    '''
    用于与多选题库交互
    多选题答案用0-3一共4bit整数表示
    4bit整数与答案对应关系为:
        0x 1 1 1 1
           D C B A
    即第0位为1表示答案A正确，0表示A不正确
    即第1位为1表示答案B正确，0表示B不正确
    ……
    答案小于1或者大于15为非法
    '''

    def __init__(self, db):
        Question.__init__(self, db, "CheckBox", 1, 15)


    def _decode(self, row):
        #answerforread = ['A', 'B', 'C', 'D']
        if row==None:
            return None
        ret=list(row)
        ret[2] = []
        for i in range(0,4):
            if ((row[2]>>i) & 0x1) == 1:
                #ret[2].append(answerforread[i])
                ret[2].append(i)
        return ret

    def _decodemany(self, rows):
        if rows==None:
            return None
        return [self._decode(row) for row in rows]


    def _encode(self, answer):
        if(len(answer)>4 or len(answer)<1):
            raise ErrAnswer("传入数据库的多选题答案个数有错误，应该是有bug!", 0)
        ret=0
        for i in range(0, len(answer)):
            if answer[i]<0 or answer[i]>3:
                raise ErrAnswer("传入数据库的多选题答案取值范围有错误，应该是有bug!", 1)
            ret = ret | 0x1<<answer[i]
        return ret

    def _encodemany(self, rows):
        ret = []
        for row in rows:
            if len(row) == 2:
                #insertmany()调用
                ret.append((row[0], self._encode(row[1])))
            else:
                #modifymany()调用
                ret.append((row[0], row[1], self._encode(row[2])))
        return ret

    def fetch(self, id_key):
        '''
        从数据库中读取一条记录
        '''
        return self._decode(Question.fetch(self, id_key))

    def fetchall(self):
        '''
        从数据库中读取所有没放入回收站的记录
        '''
        return self._decodemany(Question.fetchall(self))

    def fetchallintrash(self):
        '''
        从数据库中读取所有没放入回收站的记录
        '''
        return self._decodemany(Question.fetchallintrash(self))

    def insert(self, question, answer):
        Question.insert(self, question, self._encode(answer))

    def insertmany(self, rows):
        '''
        Insert more than one row in table self.tablename
        the parameter 'rows' should look like:
        [(question1, [0,2]), (question2, [1,2,3]), ...]
        '''
        Question.insertmany(self, self._encodemany(rows))

    def modify(self, id_key, question, answer):
        Question.modify(self, id_key, question, self._encode(answer))

    def modifymany(self, keyanstuples):
        '''
        keyanstuples should have the from:
            [(key1, qst1, ans1), (key2, qst2,  ans2), ……]
        '''
        Question.modifymany(self, self._encodemany(keyanstuples))

