#! /usr/bin/env python
#-*- coding:utf-8 -*-

from PyQt4 import QtGui,QtCore
from IdListWidget import IdListWidget
from StackAnsWidget import StackAnsWidget
import random

class Signal(QtCore.QObject):
    inExercise = QtCore.pyqtSignal(bool)

class Exam(QtGui.QWidget):
    defaultScore = [1,2,1]
    defaultCntInKind = [30, 20, 30]

    def __init__(self, dbRadioBox, dbCheckBox, dbTrFls, dbConfig, parent=None):
        super(Exam, self).__init__(parent)

        self.dbRadioBox = dbRadioBox
        self.dbCheckBox = dbCheckBox
        self.dbTrFls = dbTrFls
        self.dbTable = [self.dbRadioBox, self.dbCheckBox, self.dbTrFls]
        self.dbConfig = dbConfig
        self.signal = Signal()
        self.exerCnt = 0
        self.initUI()
        self.setMinimumSize(800, 600)

    def initUI(self):
        self.takeExerciseBtn = QtGui.QPushButton(self.tr('Product Exercises'))
        self.qstScan = QtGui.QLabel()
        self.stackAnsWidget = StackAnsWidget()
        self.idListWidget = IdListWidget()
        self.preBtn = QtGui.QPushButton(self.tr('<< Pre'))
        self.nextBtn = QtGui.QPushButton(self.tr('Next >>'))
        self.commitBtn = QtGui.QPushButton(self.tr('Commit'))
        self.takeExerciseBtn.setFixedSize(150, 50)
        self.commitBtn.setFixedSize(150, 50)

        self.takeExerciseBtn.clicked.connect(self.productExcercise)
        self.preBtn.clicked.connect(self.preExer)
        self.nextBtn.clicked.connect(self.nextExer)
        self.commitBtn.clicked.connect(self.commit)
        self.idListWidget.signal.selectNewQuestion.connect(self.setExerId)
        self.enableProductExercise()
        self.stackAnsWidget.hide()
        self.idListWidget.hide()
        self.nextBtn.hide()
        self.preBtn.hide()
        self.commitBtn.hide()

        vLayout = QtGui.QVBoxLayout()
        vLayout.addWidget(self.takeExerciseBtn)
        vLayout.addWidget(self.qstScan)
        vLayout.addItem(QtGui.QSpacerItem(1, 10,
                                          QtGui.QSizePolicy.Minimum,
                                          QtGui.QSizePolicy.Expanding))
        vLayout.addWidget(self.stackAnsWidget)
        vLayout.addWidget(self.idListWidget)
        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(self.preBtn)
        hLayout.addWidget(self.nextBtn)
        hLayout.addItem(QtGui.QSpacerItem(100, self.commitBtn.height(),
                                          QtGui.QSizePolicy.Expanding,
                                          QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(self.commitBtn)
        vLayout.addLayout(hLayout)
        self.setLayout(vLayout)

    def productExcercise(self):
        self.takeExerciseBtn.hide()
        self.commitBtn.show()
        self.idListWidget.show()
        self.preBtn.show()
        self.nextBtn.show()
        self.stackAnsWidget.show()
        self.signal.inExercise.emit(True)

        self.needTag=False
        self.selectExers()
        self.setExerId(0, 0)
        self.idListWidget.setCellCnt(self.exerCnt)
        self.idListWidget.setTagsAll()
        self.idListWidget.setSelectCell(0)

    def selectExers(self):
        #TODO
        #解析配置文件
        self.exerCntInKind = self.getConfigInDatabase('exercount', Exam.defaultCntInKind)
        idKeyLists = [[], [], []]
        for i in range(0, 3):
            ids = self.dbTable[i].fetchallIds()
            if ids != None:
                idKeyLists[i] = ids
            if self.exerCntInKind[i]>len(idKeyLists[i]):
                self.exerCntInKind[i]=len(idKeyLists[i])

        self.exerIdList = []
        for idKeyList in idKeyLists:
            for i in range(0, len(idKeyList)):
                index = random.randint(0, len(idKeyList)-1)
                idKeyList[i], idKeyList[index] = idKeyList[index], idKeyList[i]
        for i in range(0, 3):
            for idkey in idKeyLists[i][0:self.exerCntInKind[i]]:
                self.exerIdList.append((i, idkey))

        self.recordList = [self.dbTable[item[0]].fetch(item[1]) for
                            item in self.exerIdList]
        self.writeExer2File()
        #self.recordList = [None for exer in self.exerIdList]
        self.answerList = [None for exer in self.exerIdList]
        self.exerCnt = len(self.exerIdList)

    def commit(self):
        self.answerList[self.idListWidget.getSelectedCellId()]\
                = self.stackAnsWidget.getAnswer()
        self.commitBtn.hide()
        self.stackAnsWidget.hide()
        self.preBtn.hide()
        self.idListWidget.hide()
        self.nextBtn.hide()
        self.takeExerciseBtn.show()
        self.signal.inExercise.emit(False)
        score = self.getScore()
        if score >= 90:
            msg = self.tr("Congratuations! You got: ")
        else:
            msg = self.tr("You got: " )
        self.qstScan.setText(u"%s %s" % (msg, score))

    #本函数需要响应 IdListWidget 中的 selectNewQuestion信号
    #所以不能在里面调用self.idListWidget.setSelectCell()
    def setExerId(self, prevId, newId):
        if prevId !=newId:
            self.answerList[prevId] = self.stackAnsWidget.getAnswer()
            if self.answerList[prevId] == None:
                self.idListWidget.setTagOnCell(prevId)
            else:
                self.idListWidget.rstTagOnCell(prevId)

        if self.idListWidget.getSelectedCellId()>0:
            self.preBtn.setEnabled(True)
            if self.idListWidget.getSelectedCellId()>=self.exerCnt-1:
                self.nextBtn.setEnabled(False)
        if self.idListWidget.getSelectedCellId()<self.exerCnt-1:
            self.nextBtn.setEnabled(True)
            if self.idListWidget.getSelectedCellId()<=0:
                self.preBtn.setEnabled(False)
        index = self.exerIdList[newId][0]
        if self.recordList[newId]==None:
            record = self.dbTable[index].fetch(self.exerIdList[newId][1])
            self.recordList[newId] = record
        self.qstScan.setText(self.recordList[newId][1])
        self.stackAnsWidget.setCurrentIndex(index)
        self.stackAnsWidget.rstAnswer()
        if self.answerList[newId] != None:
            self.stackAnsWidget.setAnswer(self.exerIdList[newId][0],
                                          self.answerList[newId])

    def nextExer(self):
        exerId = self.idListWidget.getSelectedCellId()+1
        self.idListWidget.setSelectCell(exerId)

    def preExer(self):
        exerId = self.idListWidget.getSelectedCellId()-1
        self.idListWidget.setSelectCell(exerId)


    def getScore(self):
        scoreConfig = self.getConfigInDatabase('score', Exam.defaultScore)
        baseScore = 100/(self.exerCntInKind[0]*scoreConfig[0] +
                         self.exerCntInKind[1]*scoreConfig[1] +
                         self.exerCntInKind[2]*scoreConfig[2])
        length = len(self.recordList)
        score = 0
        for i in range(length):
            if self.recordList[i] != None \
                    and  self.recordList[i][2] == self.answerList[i]:
                score += baseScore*scoreConfig[self.exerIdList[i][0]]
        return score

    def getConfigInDatabase(self, key, default):
        valueStr = self.dbConfig.readkey(key)
        if valueStr == None:
            return default
        valueStr = valueStr.strip().split(':')
        try:
            value = [int(item) for item in valueStr]
            if len(value) != len(self.dbTable):
                    raise ValueError("Config table in database is wrong")
        except ValueError as e:
            #TODO
            #如果到达这里，则数据库应该是出了一点问题
            print(e)
            value = default
        return value

    def enableProductExercise(self):
        totalCount = 0
        for table in self.dbTable:
            totalCount += table.count()
        if totalCount == 0:
            self.takeExerciseBtn.setEnabled(False)
        else:
            self.takeExerciseBtn.setEnabled(True)

    def writeExer2File(self):
        i = 1
        with open('output/exer.txt', 'w') as f1:
            for record in self.recordList:
                f1.write((u'%s. ' % i).encode('utf8'))
                f1.write(record[1].encode('utf8'))
                f1.write('\n\n\n')
                i += 1

        indexInExers = self.tr("Index in exam:   ")
        indexInlib   = self.tr("Index in Library:")
        answer       = self.tr("Answer:          ")
        with open('output/answer.txt', 'w') as f2:
            for index in range(0, len(self.recordList)):
                indexInExers = u"%s%5s" % (indexInExers, index+1)
                indexInlib = u"%s%5s" % (indexInlib, self.recordList[index][0])
                answer = u"%s%5s" % (answer,
                                     self.decodeAnswer(
                                         self.exerIdList[index][0],
                                         self.recordList[index][2]))
                if (index+1) % 5 == 0:
                    f2.write((u"%s\n" % indexInExers).encode('utf8'))
                    f2.write((u"%s\n" % indexInlib).encode('utf8'))
                    f2.write((u"%s\n" % answer).encode('utf8'))
                    f2.write("\n")
                    indexInlib   = self.tr("Index in Library:")
                    indexInExers = self.tr("Index in exam:   ")
                    answer       = self.tr("Answer:          ")
                index += 1
            if index % 5 != 0:
                f2.write((u"%s\n" % indexInExers).encode('utf8'))
                f2.write((u"%s\n" % indexInlib).encode('utf8'))
                f2.write((u"%s\n" % answer).encode('utf8'))



    def decodeAnswer(self, kind, answer):
        answerforread = [['A', 'B', 'C', 'D'],
                         ['A', 'B', 'C', 'D'],
                         [u'√', u'×']]
        if kind == 1:
            ret = ""
            for i in answer:
                ret = "%s%s" % (ret, answerforread[1][i])
            return ret
        else:
            return answerforread[kind][answer]
