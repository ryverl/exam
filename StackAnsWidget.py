#!/usr/bin/env python
# -*- coding:utf-8 -*-

from PyQt4 import QtGui

class StackAnsWidget(QtGui.QStackedWidget):

    def __init__(self, parent=None):
        super(StackAnsWidget, self).__init__(parent)
        self.initUI()

    def initUI(self):
        self.setMaximumHeight(50)
        self.answerGrp = [QtGui.QButtonGroup(),
                          QtGui.QButtonGroup(),
                          QtGui.QButtonGroup()]

        self.insertWidget(0, self.createRadioBtnAnswer())
        self.insertWidget(1, self.createCheckBoxAnswer())
        self.insertWidget(2, self.createTrFlsAnswer())

    def createRadioBtnAnswer(self):
        widget = QtGui.QWidget()
        widget.setSizePolicy(QtGui.QSizePolicy.Expanding,
                             QtGui.QSizePolicy.Minimum)
        a = QtGui.QRadioButton("A")
        b = QtGui.QRadioButton("B")
        c = QtGui.QRadioButton("C")
        d = QtGui.QRadioButton("D")

        self.answerGrp[0].addButton(a, 0)
        self.answerGrp[0].addButton(b, 1)
        self.answerGrp[0].addButton(c, 2)
        self.answerGrp[0].addButton(d, 3)

        spacerItem = QtGui.QSpacerItem(self.width()*2/4, 1,
                                       QtGui.QSizePolicy.Expanding)
        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(a)
        hLayout.addItem(QtGui.QSpacerItem(50, 1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(b)
        hLayout.addItem(QtGui.QSpacerItem(50, 1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(c)
        hLayout.addItem(QtGui.QSpacerItem(50, 1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(d)
        hLayout.addItem(spacerItem)
        widget.setLayout(hLayout)
        return widget

    def createCheckBoxAnswer(self):
        widget = QtGui.QWidget()
        widget.setSizePolicy(QtGui.QSizePolicy.Fixed,
                             QtGui.QSizePolicy.Minimum)
        a = QtGui.QCheckBox("A")
        b = QtGui.QCheckBox("B")
        c = QtGui.QCheckBox("C")
        d = QtGui.QCheckBox("D")

        self.answerGrp[1].addButton(a, 0)
        self.answerGrp[1].addButton(b, 1)
        self.answerGrp[1].addButton(c, 2)
        self.answerGrp[1].addButton(d, 3)
        self.answerGrp[1].setExclusive(False)

        spacerItem = QtGui.QSpacerItem(self.width()*2/4, 1,
                                       QtGui.QSizePolicy.Expanding)
        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(a)
        hLayout.addItem(QtGui.QSpacerItem(50, 1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(b)
        hLayout.addItem(QtGui.QSpacerItem(50, 1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(c)
        hLayout.addItem(QtGui.QSpacerItem(50, 1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(d)
        hLayout.addItem(spacerItem)
        widget.setLayout(hLayout)
        return widget

    def createTrFlsAnswer(self):
        widget = QtGui.QWidget()
        widget.setSizePolicy(QtGui.QSizePolicy.Fixed,
                             QtGui.QSizePolicy.Minimum)
        true = QtGui.QRadioButton(self.tr("True"))
        false = QtGui.QRadioButton(self.tr("False"))

        self.answerGrp[2] = QtGui.QButtonGroup()
        self.answerGrp[2].addButton(true, 0)
        self.answerGrp[2].addButton(false, 1)

        spacerItem = QtGui.QSpacerItem(self.width()*2/4, 1,
                                       QtGui.QSizePolicy.Expanding)
        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(true)
        hLayout.addItem(QtGui.QSpacerItem(50, 1, QtGui.QSizePolicy.Fixed))
        hLayout.addWidget(false)
        hLayout.addItem(spacerItem)
        widget.setLayout(hLayout)
        return widget

    def getAnswer(self):
        answer= self.answerGrp[self.currentIndex()].checkedId()
        if answer == -1:
            return None

        #对于多选题checkId()只返回最后一个设置为有效的选项
        #因此需要特殊处理
        if self.currentIndex()==1:
            answer = []
            for i  in range(0, 4):
                if self.answerGrp[1].button(i).isChecked():
                    answer.append(i)

        self.rstAnswer()
        return answer

    def rstAnswer(self):
        exclusive = [True, False, True]
        for i in range(0, 3):
            self.answerGrp[i].setExclusive(False)
            for button  in self.answerGrp[i].buttons():
                button.setChecked(False)
            self.answerGrp[i].setExclusive(exclusive[i])

    def setAnswer(self, kindId, answer):
        #单选或者判断的答案为直接的数字
        if kindId != 1:
            self.answerGrp[kindId].button(answer).setChecked(True)
        #多选返回的为类似[0,1,2]的数组
        else:
            #把原来的选项清零
            for button  in self.answerGrp[kindId].buttons():
                button.setChecked(False)
            for i in answer:
                self.answerGrp[kindId].button(i).setChecked(True)
