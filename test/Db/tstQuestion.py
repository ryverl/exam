#!/usr/bin/env python
# -*- coding: utf-8 -*-

#关于modify(), modifymany()的测试在tstCheckBox.py中进行


import sys
sys.path.append('../..')
sys.path.append('.')

import Db
import Db.Question

db = Db.Db('data.db')
question = Db.Question.Question(db, 'Question')
db.cursor.execute('DELETE  FROM Question')
db.commit()
assert(question.count()==0)
assert(question.countintrash()==0)
assert(question.totalcount()==0)
print('After created: Right!')

question.insert(unicode(u'问题1'), 4)
assert(question.count()==1)
assert(question.countintrash()==0)
assert(question.totalcount()==1)
print('After inserted: Right!')

question.insertmany([(u'问题2', 2),
                     (u'问题3', 1),
                     (u'问题4', 4),
                     (u'问题5', 1),
                     (u'问题6', 4),
                     (u'问题7', 3)])
assert(question.count()==7)
assert(question.countintrash()==0)
assert(question.totalcount()==7)
print('After insertmany: Right!')

question.movetotrash(2)
question.movetotrashmany([3, 4, 5, 6])
assert(question.count()==2)
assert(question.countintrash()==5)
assert(question.totalcount()==7)
print('After movetotrash: Right!')

question.recover(3)
question.recovermany([2, 4])
assert(question.count()==5)
assert(question.countintrash()==2)
assert(question.totalcount()==7)
print('After recover: Right!')

question.delete(2)
question.deletemany([2, 6])
assert(question.count()==4)
assert(question.countintrash()==1)
assert(question.totalcount()==5)
print('After delete: Right!')

assert(question.maxid()==7)
assert(question.minid()==1)
print('maxid() and minid() right!')

item1 = question.fetch(1)
item2 = question.fetch(2)
assert(item1[0]==1)
assert(item1[1]==u'问题1')
assert(item1[2]==4)
assert(item1[3]==0)
assert(item2 == None)
print('fetch() right!')

items=question.fetchall()
assert(items[0][0]==1)
assert(items[0][1]==u'问题1')
assert(items[0][2]==4)
assert(items[0][3]==0)
assert(items[1][0]==3)
assert(items[1][1]==u'问题3')
assert(items[1][2]==1)
assert(items[1][3]==0)
assert(items[2][0]==4)
assert(items[3][0]==7)
print('fetchall() right!')

items=question.fetchallintrash()
assert(items[0][0]==5)
assert(items[0][3]==1)
print('fetchallintrash() right!')

db.cursor.execute('DELETE  FROM Question')
question = Db.Question.Question(db, 'Question', 3, None)
question.insert(u'问题1', 1)
question.insert(u'问题2', 2)
question.insert(u'问题3', 3)
question.insert(u'问题4', 4)
question.insert(u'问题5', 5)

try:
    question.checkanswerstatus()
    assert(1==1)
except Db.Question.IllStatus as e:
    print(e)
    assert(question.itemwithillans ==
            [(1,1), (2,2)])
    print(u"checkanswerstatus()可以正确处理只有下限存在的情况")

db.cursor.execute('DELETE  FROM Question')
question = Db.Question.Question(db, 'Question', None, 2)
question.insert(u'问题1', 1)
question.insert(u'问题2', 2)
question.insert(u'问题3', 3)
question.insert(u'问题4', 4)
question.insert(u'问题5', 5)

try:
    question.checkanswerstatus()
    assert(1==1)
except Db.Question.IllStatus as e:
    assert(question.itemwithillans ==
            [(3,3), (4,4), (5,5)])
    print(u"checkanswerstatus()可以正确处理只有上限存在的情况")

db.cursor.execute('DELETE  FROM Question')
question = Db.Question.Question(db, 'Question', 2, 3)
question.insert(u'问题1', 1)
question.insert(u'问题2', 2)
question.insert(u'问题3', 3)
question.insert(u'问题4', 4)
question.insert(u'问题5', 5)

try:
    question.checkanswerstatus()
    assert(1==1)
except Db.Question.IllStatus as e:
    assert(question.itemwithillans ==
            [(1,1), (4,4), (5,5)])
    print(u"checkanswerstatus()可以正确处理上下限同时存在的情况")

db.close()
