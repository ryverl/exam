#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
作为某一类题型的通用模型

目前还没找到通过python变量来指定表名的合适方法
因此暂时采用python string operator来操作

计划后续实现中使用sqlalchemy来替代
或者使用其他可防止sql注入攻击的方式
'''

class IllParaErr(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)

class IllStatus(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)

class Question:

    def __init__(self, db, tablename, answerlow=None, answerup=None):
        self.db = db

        #因为目前没有找到有效的通过python变量来指定表名的方法
        #而这里使用的python string operator易受sql注入攻击
        #这里做一个简单的检查
        #检查的规范是tablename必须是str类型，且只能由拉丁字母构成
        #不允许有空格或其它字符
        #即只能是[a~z]和[A~Z]
        if not type(tablename) == type(""):
            raise IllParaErr("the table name should be string")
        for i in tablename:
            if not ((i>='a' and i<='z') or (i>='A' and i<='Z')):
                raise IllParaErr("only latin character is allowed in table name")

        self.tablename = tablename
        #intrash=0表示该记录没有被放入回收站
        #否则表示该记录已被删除
        instruct = 'CREATE TABLE IF NOT EXISTS\
                    %s(id INTEGER PRIMARY KEY, \
                    question TEXT,\
                    answer INTEGER,\
                    intrash INTEGER DEFAULT 0)' % self.tablename
        self.db.cursor.execute(instruct)
        self.db.commit()
        #用于记录题库中答案处于非法状态的题目的id列表
        self.answerlow = answerlow
        self.answerup = answerup
        self.itemwithillans = []
        if self.answerlow != None or self.answerup != None:
            self.checkanswerstatus()

    def checkanswerstatus(self):
        instruct = 'SELECT id, answer from %s WHERE' % self.tablename
        loweff = False
        thrds = []
        if self.answerlow != None:
            instruct = '%s answer<?' % instruct
            thrds.append(self.answerlow)
            loweff = True
        if self.answerup != None:
            if loweff:
                instruct = '%s OR' % instruct
            instruct = '%s answer>?' % instruct
            thrds.append(self.answerup)
        self.db.cursor.execute(instruct, thrds)
        self.itemwithillans = self.db.cursor.fetchall()
        if (len(self.itemwithillans)>0):
            raise IllStatus("喔，数据库好像出了点问题，有一部分题目答案处于非法状态!")

    def modify(self, id_key, question, answer):
        instruct = 'SELECT id from %s WHERE id=?' % self.tablename
        self.db.cursor.execute(instruct, (id_key,))
        ret = self.db.cursor.fetchone()
        if ret != None:
            instruct = 'UPDATE %s SET question=?, answer=?\
                        WHERE id=?' % self.tablename
        else:
            instruct = 'INSERT INTO %s(question, answer, id) \
                        VALUES(?, ? ,?)' % self.tablename
        self.db.cursor.execute(instruct, (question, answer, id_key))
        self.db.commit()

    def modifymany(self, keyqstanstuples):
        '''
        keyquestanstuples should have the from:
            [(key1, qst1, ans1), (key2, qst2, ans2), ……]
        '''
        instruct = 'UPDATE %s SET question=?, answer=?\
                    WHERE id=?' % self.tablename
        self.db.cursor.executemany(instruct,
                                   [(pair[1], pair[2], pair[0]) \
                                           for pair in keyqstanstuples])
        self.db.commit()

    def count(self):
        instruct = 'SELECT COUNT(*) FROM %s \
                    WHERE NOT intrash' % self.tablename
        self.db.cursor.execute(instruct)
        return self.db.cursor.fetchone()[0]

    def countintrash(self):
        instruct = 'SELECT COUNT(*) FROM %s \
                    WHERE intrash' % self.tablename
        self.db.cursor.execute(instruct)
        return self.db.cursor.fetchone()[0]

    def totalcount(self):
        instruct = 'SELECT COUNT(*) FROM %s' % self.tablename
        self.db.cursor.execute(instruct)
        return self.db.cursor.fetchone()[0]

    def maxid(self):
        instruct = 'SELECT id FROM %s \
                    ORDER BY id DESC \
                    LIMIT 1' % self.tablename
        self.db.cursor.execute(instruct)
        ids = self.db.cursor.fetchone()
        if ids != None:
            return ids[0]
        else:
            return 0


    def minid(self):
        instruct = 'SELECT id FROM %s \
                    ORDER BY id \
                    LIMIT 1' % self.tablename
        self.db.cursor.execute(instruct)
        ids = self.db.cursor.fetchone()[0]
        if ids != None:
            return ids[0]
        else:
            return 0

    def insert(self, question, answer):
        instruct = 'INSERT INTO %s(question, answer) \
                    VALUES (?, ?)' % self.tablename
        self.db.cursor.execute(instruct, (question, answer))
        self.db.commit()

    def insertmany(self, rows):
        '''
        Insert more than one row in table self.tablename
        the parameter 'rows' should look like:
        [(question1, answer1), (question2, answer2), ...]
        '''
        instruct = 'INSERT INTO %s(question, answer)\
                    VALUES (?, ?)' % self.tablename
        self.db.cursor.executemany(instruct,  rows)
        self.db.commit()

    def movetotrash(self, id_key):
        '''
        将一条记录放入回收站
        '''
        instruct = 'UPDATE %s SET intrash=1 WHERE id=?' % self.tablename
        self.db.cursor.execute(instruct,(id_key,))
        self.db.commit()

    def movetotrashmany(self, id_keys):
        '''
        将一条记录放入回收站
        id_keys必须形如[id_key1, id_key2, ……]
        '''
        instruct = 'UPDATE %s SET intrash=1 WHERE id=?' % self.tablename
        self.db.cursor.executemany(instruct, [(i, ) for i in id_keys] )
        self.db.commit()

    def delete(self, id_key):
        '''
        彻底删除某一记录
        '''
        instruct = 'DELETE from %s WHERE id=?'% self.tablename
        self.db.cursor.execute(instruct, (id_key,))
        self.db.commit()

    def deletemany(self, id_keys):
        '''
        彻底删除多条记录
        id_keys必须形如[id_key1, id_key2, ……]
        '''
        instruct = 'DELETE from %s WHERE id=?' % self.tablename
        self.db.cursor.executemany(instruct, [(i, ) for i in id_keys] )
        self.db.commit()

    def recover(self, id_key):
        '''
        从回收站恢复某一记录
        '''
        instruct = 'UPDATE %s SET intrash=0 WHERE id=?' % self.tablename
        self.db.cursor.execute(instruct, (id_key, ))
        self.db.commit()

    def recovermany(self, id_keys):
        '''
        从回收站恢复多条记录
        id_keys必须形如[id_key1, id_key2, ……]
        '''
        instruct = 'UPDATE %s SET intrash=0 WHERE id=?' % self.tablename
        self.db.cursor.executemany(instruct,[(i, ) for i in id_keys] )
        self.db.commit()

    def fetch(self, id_key):
        '''
        从数据库中读取一条记录
        '''
        instruct = 'SELECT * FROM %s WHERE id=?' % self.tablename
        self.db.cursor.execute(instruct, (id_key, ))
        return self.db.cursor.fetchone()


    def fetchall(self ):
        '''
        从数据库中读取所有没放入回收站的记录
        '''
        instruct = 'SELECT * FROM %s WHERE NOT intrash' % self.tablename
        self.db.cursor.execute(instruct)
        return self.db.cursor.fetchall()

    def fetchallintrash(self ):
        '''
        从数据库中读取所有没放入回收站的记录
        '''
        instruct = 'SELECT * FROM %s WHERE intrash' % self.tablename
        self.db.cursor.execute(instruct)
        return self.db.cursor.fetchall()

    def fetchallIds(self):
        '''
        从数据库中读取所有没放入回收站的记录的ID集合
        '''
        instruct = 'SELECT id FROM %s WHERE NOT intrash' % self.tablename
        self.db.cursor.execute(instruct)
        records = self.db.cursor.fetchall()
        if len(records) != 0:
            return [record[0] for record in records]
        else:
            return None
