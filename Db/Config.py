#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
处理配置参数
'''

class Config:

    def __init__(self, db):
        self.db = db
        instruct = 'CREATE TABLE IF NOT EXISTS\
                    Config(id INTEGER PRIMARY KEY, \
                    key TEXT,\
                    value TEXT)'
        self.db.cursor.execute(instruct)
        self.db.commit()

    def setkey(self, key, value):
        instruct = 'SELECT key FROM Config \
                    WHERE key=?'
        self.db.cursor.execute(instruct, (key,))
        ret = self.db.cursor.fetchall()
        if len(ret) == 0:
            instruct = 'INSERT INTO Config(value, key) \
                        VALUES(?, ?)'
        else:
            instruct = 'UPDATE Config SET value=? \
                        WHERE key=?'
        self.db.cursor.execute(instruct, (value, key))
        self.db.commit()

    def readkey(self, key):
        instruct = 'SELECT value FROM Config \
                    WHERE key=?'
        self.db.cursor.execute(instruct, (key,))
        ret =  self.db.cursor.fetchall()
        if len(ret)==0:
            return None
        else:
            return ret[0][0]
