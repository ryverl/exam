#! /usr/bin/env python
# -*- coding:utf-8 -*-

from PyQt4.QtGui import QTabWidget
from PyQt4 import QtCore

import Exam
import Scan
import Insert

class Signal(QtCore.QObject):
    setStatusMsg = QtCore.pyqtSignal(str)


class TabWidget(QTabWidget):

    def __init__(self, dbRadioBox, dbCheckBox, dbTrFls, dbConfig, parent=None):
        super(TabWidget, self).__init__(parent)
        self.signal = Signal()
        self.initUI(dbRadioBox, dbCheckBox, dbTrFls, dbConfig)

    def initUI(self, dbRadioBox, dbCheckBox, dbTrFls, dbConfig):
        self.exam = Exam.Exam(dbRadioBox, dbCheckBox, dbTrFls, dbConfig)
        self.scan = Scan.Scan(dbRadioBox, dbCheckBox, dbTrFls)
        self.insert = Insert.Insert(dbRadioBox, dbCheckBox, dbTrFls)
        self.addTab(self.exam, self.tr("Local Exam"))
        self.addTab(self.scan, self.tr("Scan Library"))
        self.addTab(self.insert, self.tr("Insert Into Library"))

        self.exam.signal.inExercise[bool].connect(self.inExercise)
        self.scan.signal.modifyQst.connect(self.modifyQst)
        self.scan.signal.deleteSomeExer.connect(self.exam.enableProductExercise)
        self.insert.signal.databaseUpdate.connect(self.scan.databaseUpdate)
        self.insert.signal.databaseUpdate.connect(self.exam.enableProductExercise)

    def modifyQst(self, kindId, qstId):
        self.setCurrentWidget(self.insert)
        self.insert.changeQst(kindId, qstId)

    def inExercise(self, flag):
        if flag:
            self.removeTab(self.indexOf(self.scan))
            self.removeTab(self.indexOf(self.insert))
        else:
            self.addTab(self.scan, self.tr("Scan Library"))
            self.addTab(self.insert, self.tr("Insert Into Library"))
