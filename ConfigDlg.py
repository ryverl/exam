#!/usr/bin/env python
# -*- coding:utf-8 -*-

from PyQt4 import QtGui,QtCore

class ConfigDlg(QtGui.QDialog):
    defaultScore = [1, 2, 1]
    defaultCnt   = [30, 20, 30]

    def __init__(self, dbRadioBox, dbCheckBox, dbTrFls,  dbConfig, parent=None):
        super(ConfigDlg, self).__init__()
        self.dbConfig = dbConfig
        self.dbTable = [dbRadioBox, dbCheckBox, dbTrFls]
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.tr("Config"))

        self.score = self.getConfigInDatabase('score',
                                              ConfigDlg.defaultScore)
        self.exerCnt = self.getConfigInDatabase('exercount',
                                                ConfigDlg.defaultCnt)
        for i in range(0, len(self.dbTable)):
            if self.exerCnt[i]>self.dbTable[i].count():
                self.exerCnt[i] = self.dbTable[i].count()

        self.scoreInputs = [None, None, None]
        self.cntInputs =   [None, None, None]
        for i in range(0,3):
            self.scoreInputs[i] = QtGui.QSpinBox()
            self.scoreInputs[i].setValue(self.score[i])
            self.scoreInputs[i].setFixedSize(80, 30)
            self.cntInputs[i] = QtGui.QSpinBox()
            self.cntInputs[i].setValue(self.exerCnt[i])
            self.cntInputs[i].setFixedSize(80, 30)

        self.OkBtn = QtGui.QPushButton(self.tr("OK"))
        self.cancelBtn = QtGui.QPushButton(self.tr("Cancel"))
        self.cancelBtn.clicked.connect(self.close)
        self.OkBtn.clicked.connect(self.Ok)

        #   -----------------------------
        #   |     score parameter       |
        #   -----------------------------
        #   | label1 | lable2 | label3  |
        #   -----------------------------
        #   | input  | input  | input   |
        #   -----------------------------
        #
        #
        #   -----------------------------
        #   |    number of exercisers   |
        #   -----------------------------
        #   | label1 | lable2 | label3  |
        #   -----------------------------
        #   | input  | input  | input   |
        #   -----------------------------
        hLayout1  = QtGui.QHBoxLayout()
        hLayout1.addWidget(QtGui.QLabel(self.tr("Score Parameter")))
        gridLayout = QtGui.QGridLayout()
        row = 1; col = 1; rowSpan = 1; colSpan = 3
        gridLayout.addLayout(hLayout1, row, col, rowSpan, colSpan)
        gridLayout.setAlignment(hLayout1, QtCore.Qt.AlignCenter)
        gridLayout.addWidget(QtGui.QLabel(self.tr("SingleSelect:")), 2, 1)
        gridLayout.addWidget(QtGui.QLabel(self.tr("MultiSelect:") ), 2, 2)
        gridLayout.addWidget(QtGui.QLabel(self.tr("TrueOrFalse:")), 2, 3)
        gridLayout.addWidget(self.scoreInputs[0], 3, 1)
        gridLayout.addWidget(self.scoreInputs[1], 3, 2)
        gridLayout.addWidget(self.scoreInputs[2], 3, 3)

        msg = self.tr("In Fact, the score parameter above\n"   ) +\
              self.tr("is only a reference value. When the\n " ) +\
              self.tr("parameters you set makes the total \n"  ) +\
              self.tr("score not equal to 100, I will adjust\n") +\
              self.tr("it, but keep the ratio of the scores\n" ) +\
              self.tr("as you set.")


        hLayout2 = QtGui.QHBoxLayout()
        hLayout2.addWidget(QtGui.QLabel(msg))
        row = 4; col = 1; rowSpan = 1; colSpan = 3
        gridLayout.addLayout(hLayout2, row, col, rowSpan, colSpan)
        gridLayout.setAlignment(hLayout2, QtCore.Qt.AlignCenter)
        gridLayout.addItem(QtGui.QSpacerItem(1, 30,
                                             QtGui.QSizePolicy.Fixed,
                                             QtGui.QSizePolicy.Fixed))

        hLayout3  = QtGui.QHBoxLayout()
        hLayout3.addWidget(QtGui.QLabel(self.tr("Number Of Exercisers")))
        row = 6; col = 1; rowSpan = 1; colSpan = 3
        gridLayout.addLayout(hLayout3, row, col, rowSpan, colSpan)
        gridLayout.setAlignment(hLayout3, QtCore.Qt.AlignCenter)
        gridLayout.addWidget(QtGui.QLabel(self.tr("SingleSelect:")), 7, 1)
        gridLayout.addWidget(QtGui.QLabel(self.tr("MultiSelect:") ), 7, 2)
        gridLayout.addWidget(QtGui.QLabel(self.tr("TrueOrFalse:")),  7, 3)
        gridLayout.addWidget(self.cntInputs[0], 8, 1)
        gridLayout.addWidget(self.cntInputs[1], 8, 2)
        gridLayout.addWidget(self.cntInputs[2], 8, 3)

        gridLayout.addWidget(self.OkBtn, 9, 2)
        gridLayout.addWidget(self.cancelBtn, 9, 3)

        self.setLayout(gridLayout)

    def Ok(self):
        outOfRange = []
        msg = [self.tr('"SingleSelect"'),
               self.tr('"MultiSelect"'),
               self.tr('"TrueOrFalse"')]
        for i in range(0, len(self.score)):
            self.score[i] = self.scoreInputs[i].value()
            self.exerCnt[i] = self.cntInputs[i].value()
            if self.exerCnt[i] > self.dbTable[i].count():
                self.exerCnt[i]=self.dbTable[i].count()
                outOfRange.append(i)
        if len(outOfRange) != 0:
            text = self.tr("There are not enouth number of")
            for i in outOfRange:
                text = text + " " + msg[i]
            text = text + self.tr(" exercisers, So I will use")
            for i in outOfRange:
                text = text + " " + unicode(self.dbTable[i].count())
            if len(outOfRange)>1:
                text = text + self.tr(" respectively!")
            text = text + self.tr(", Are you sure?")
            ok = QtGui.QMessageBox.question(self,
                                            "Out of Range",     #Title
                                            text,
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                            QtGui.QMessageBox.No)
            if ok == QtGui.QMessageBox.No:
                return
        scoreStr = '%s:%s:%s' % (self.score[0], self.score[1], self.score[2])
        exerCntStr =  '%s:%s:%s' % (self.exerCnt[0], self.exerCnt[1], self.exerCnt[2])
        self.dbConfig.setkey('score', scoreStr)
        self.dbConfig.setkey('exercount', exerCntStr)
        self.close()
        pass

    def getConfigInDatabase(self, key, default):
        valueStr = self.dbConfig.readkey(key)
        if valueStr == None:
            return default
        valueStr = valueStr.strip().split(':')
        try:
            value = [int(item) for item in valueStr]
            if len(value) != len(self.dbTable):
                    raise ValueError("Config table in database is wrong")
        except ValueError as e:
            #TODO
            #如果到达这里，则数据库应该是出了一点问题
            print(e)
            value = default
        return value
