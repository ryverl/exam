#! /usr/bin/env python
#-*- coding:utf-8 -*-

from PyQt4 import QtGui, QtCore
from QstKindSlct import QstKindSlct
from StackAnsWidget import StackAnsWidget

class Signal(QtCore.QObject):
    databaseUpdate = QtCore.pyqtSignal()


class Insert(QtGui.QWidget):
    '''
    录入新的试题
    '''

    def __init__(self, dbRadioGrp, dbCheckBox, dbTrFls, parent=None):
        super(Insert, self).__init__(parent)
        self.dbRadioGrp = dbRadioGrp
        self.dbCheckBox= dbCheckBox
        self.dbTrFls= dbTrFls
        self.dbTable=[self.dbRadioGrp, self.dbCheckBox, self.dbTrFls]
        #当self.commitIsInsert为True是本次提交为插入
        #否则为修改
        self.commitIsInsert = True
        self.qstId = 0
        self.signal = Signal()
        self.initUI()

    def initUI(self):
        self.kindBtnGrp = QstKindSlct()

        self.qstInput = QtGui.QTextEdit()
        self.stackAnsWidget = StackAnsWidget()
        self.commitBtn = QtGui.QPushButton(self.tr("Commit"))
        self.commitBtn.setSizePolicy(QtGui.QSizePolicy.Fixed,
                                     QtGui.QSizePolicy.Fixed)
        self.answerGrp = [QtGui.QButtonGroup(),
                          QtGui.QButtonGroup(),
                          QtGui.QButtonGroup()]
        self.kindBtnGrp.signal.slctNewKind[int].\
                connect(self.stackAnsWidget.setCurrentIndex)
        self.commitBtn.clicked.connect(self.commit)
        vLayout = QtGui.QVBoxLayout()
        vLayout.addWidget(self.kindBtnGrp)
        vLayout.addWidget(QtGui.QLabel(self.tr("Please input the whole question:")))
        vLayout.addWidget(self.qstInput)
        vLayout.addWidget(QtGui.QLabel(self.tr("Which one is the answer?")))
        vLayout.addWidget(self.stackAnsWidget)
        vLayout.addWidget(self.commitBtn)
        self.setLayout(vLayout)

    def commit(self):
        question = self.qstInput.toPlainText().trimmed()
        if len(question.toUtf8())==0:
            QtGui.QMessageBox.information(self,
                                          self.tr("You havn't input the question!"),
                                          self.tr("You havn't input the question!"))
            return
        answer = self.stackAnsWidget.getAnswer()
        if answer == None:
            QtGui.QMessageBox.information(self,
                                          self.tr("You havn't input the answer!"),
                                          self.tr("You havn't input the answer!"))
            return

        table = self.dbTable[self.kindBtnGrp.checkedId()]
        if self.commitIsInsert:
            table.insert(str(question.toUtf8()).decode('utf-8'),
                         answer)
        else:
            table.modify(self.qstId,
                         str(question.toUtf8()).decode('utf-8'),
                         answer)

        self.signal.databaseUpdate.emit()

        self.commitIsInsert = True
        self.qstInput.clear()
        #self.rstAnswer()

    def rstAnswer(self):
        exclusive = [True, False, True]
        for i in range(0, 3):
            self.answerGrp[i].setExclusive(False)
            for button  in self.answerGrp[i].buttons():
                button.setChecked(False)
            self.answerGrp[i].setExclusive(exclusive[i])

    def changeQst(self, kindId, qstId):
        self.commitIsInsert = False
        self.qstId = qstId
        record = self.dbTable[kindId].fetch(qstId)
        if record == None:
            return
        self.kindBtnGrp.setNewKind(kindId)
        self.qstInput.setPlainText(record[1])
        self.stackAnsWidget.setAnswer(kindId, record[2])
