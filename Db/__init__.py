#!/usr/bin/env python

# -*- coding:utf-8 -*-

import sqlite3

class Db():

    def __init__(self, filename):
        self.dbfile = filename
        self.conn = sqlite3.connect(self.dbfile)
        self.cursor = self.conn.cursor()

    def close(self):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()

    def commit(self):
        self.conn.commit()

__all__ = ['Question', 'CheckBox', 'RadioGroup', 'TrueFalse', 'Config']
